# /home/mhbrt/Yuna/pipeline/Flow/pipeline/connecter/src/app/server.ts

import os
import subprocess
import optparse
import shutil


parser = optparse.OptionParser()

parser.add_option('-i', '--input',
    action="store", dest="input",
    help="input filename", default="")

parser.add_option('-f', '--folder',
    action="store", dest="folder",
    help="Input folder", default="")

options, args = parser.parse_args()

input = options.input
output = input.replace(input.split('/')[-1], 'temp')

# git -C ./connecter/ log --oneline -1
cmd = ["git", "-C", options.folder, "log", "--oneline", '-1']
process = subprocess.Popen(cmd, stdout=subprocess.PIPE)
msg, error = process.communicate()
msg = msg.rstrip().decode()
if error != None:
    print(error)

string1 = 'Yuna Connecter is running fine'

with open(input) as f1, open(output, "w") as f3:
    f1_lines = f1.readlines()
    for line in f1_lines:
        if string1 in line:
            # line = line.replace(string1, string2)
            line = "                                            <p>Yuna Connecter is running fine ("+ msg +").</p> \n"
        f3.write(line)

shutil.move(output, input)