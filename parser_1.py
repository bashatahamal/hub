import optparse

parser = optparse.OptionParser()

parser.add_option('-i', '--input',
    action="store", dest="input",
    help="input filename", default="")

parser.add_option('-o', '--output',
    action="store", dest="output",
    help="output filename", default="")

parser.add_option('-k', '--key',
    action="store", dest="key",
    help="key to look for separate by , max 3", default="")

parser.add_option('-1', '--string1',
    action="store", dest="string1",
    help="string to modify", default="")

parser.add_option('-2', '--string2',
    action="store", dest="string2",
    help="string to replace", default="")

options, args = parser.parse_args()

if options.input.split('.')[-1] == 'json':
    import json, ast
    with open(options.input, "r") as fout2:
        json_data = json.load(fout2)
        json_data = ast.literal_eval(json.dumps(json_data))
        get_string = json_data[options.key.split(',')[0]]
        for x in range(1, len(options.key.split(','))):
            get_string = get_string[options.key.split(',')[x]]

        # replace the string
        get_string = get_string.replace(options.string1, options.string2)

        if len(options.key.split(',')) == 1:
            json_data[options.key.split(',')[0]] = get_string
        elif len(options.key.split(',')) == 2:
            json_data[options.key.split(',')[0]][options.key.split(',')[1]] = get_string
        elif len(options.key.split(',')) == 3:
            json_data[options.key.split(',')[0]][options.key.split(',')[1]][options.key.split(',')[2]] = get_string

    # dump json to another file
    with open(options.output, "w") as fout:
        fout.write(json.dumps(json_data))

elif options.input.split('/')[-1][:5] == '.env.':
    with open(options.input) as f1, open(options.output, "w") as f3:
        f1_lines = f1.readlines()
        for line in f1_lines:
            if options.key in line:
                if options.string1 in line:
                    line = line.replace(options.string1, options.string2)
                else:
                    line = options.key + '=' + options.string2
            f3.write(line)
else:
    pass